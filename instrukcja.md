## Change
Spams too much logs so some parts must be slenced with `> /dev/null`



### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://sourceware.org/git/glibc.git
cd glibc

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y glibc

mkdir build
cd build
../configure --disable-sanity-checks

make -j8

```
